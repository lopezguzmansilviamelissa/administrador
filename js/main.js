// Import the functions you need from the SDKs you need

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

  // Your web app's Firebase configuration
  const firebaseConfig = {
    
    apiKey: "AIzaSyAI3BQyYqzDPFEdcElQLtEKbRmSVldL9pI",
    authDomain: "proyectowebfinal-c808e.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-c808e-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-c808e",
    storageBucket: "proyectowebfinal-c808e.appspot.com",
    messagingSenderId: "1081864136515",
    appId: "1:1081864136515:web:182b643ad564bfe645b1f6"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');


var clienteinicial = 0;
var dimenciones ="";
var estilo ="";
var descripcion ="";
var urlImag ="";


function leerInputs(){
    clienteinicial = document.getElementById('txtClienteInicial').value;
    dimenciones = document.getElementById('txtDimenciones').value;
    estilo =  document.getElementById('txtEstilo').value;
    descripcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
}
function mostrarMensaje(mensaje){
    var mensajeElement = document.getElementById('mensaje')
    mensajeElement.textContent= mensaje;
    mensajeElement.style.display= 'block';
    setTimeout(()=>{
    mensajeElement.style.display ='none'},1000);
}



function limpiarInputs() {
    document.getElementById('txtClienteInicial').value = '';
    document.getElementById('txtDimenciones').value = '';
    document.getElementById('txtEstilo').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs() {

    document.getElementById('txtDimenciones').value = dimenciones;
    document.getElementById('txtEstilo').value = estilo;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}


function buscarProducto()
{
    let clienteinicial = document.getElementById('txtClienteInicial').value.trim();
    if(clienteinicial=== "") {
        mostrarMensaje("No se ingreso cliente inicila");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Comisiones/' + clienteinicial)).then((snapshot) =>{
        if(snapshot.exists()) {
            dimenciones = snapshot.val().dimenciones;
            estilo = snapshot.val().estilo;
            descripcion = snapshot.val().descripcion;
            urlImag = snapshot.val().urlImg;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("El Producto con Codigo" + clienteinicial + "No existe.");
        }
    });
}

const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);
btnBuscar.addEventListener('click', buscarProducto);

function insertarProducto(){
    alert("ingrese a add db");
    leerInputs();

    if(clienteinicial==="" || dimenciones==="" || estilo==="" || descripcion===""){
        mostrarMensaje("faltaron datos por capturar");
        return;
    }

    set(
         refS(db,'Comisiones/' + clienteinicial),
         {
            clienteinicial:clienteinicial,
            dimenciones:dimenciones,
            estilo:estilo,
            descripcion:descripcion,
            urlImag:urlImag
         }
    ).then(()=>{

        alert("Se agrego con exito");
    }).catch((error)=>{
        alert("Ocurrio un error")
 })
}

Listarproductos()
function Listarproductos() {
    const dbRef = refS(db, 'Comisiones');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbRef,(snapshot) => {
        snapshot.forEach((childSnapshot)=> {
            const childKey = childSnapshot.key;

            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.dimenciones;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.estilo;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.descripcion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImg;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true});
}


function actualizarComisiones() {
    leerInputs();
    if (clienteinicial === "" || dimenciones === "" || estilo === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion.");
        return;
    }
    alert("actualizar");
    update(refS(db, 'Comisiones/' + clienteinicial), {
        clienteinicial:clienteinicial,
        dimenciones: dimenciones,
        estilo: estilo,
        descripcion: descripcion,
        urlImg: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
}



function eliminarComisiones() {
    let clienteinicial= document.getElementById('txtClienteInicial').value.trim();
    if (clienteinicial === "") {
        mostrarMensaje("No se ingreso un Codigo válido.");
        return;
    }
    const dbref = refS(db);
    get(child(dbref, 'Comisiones/' + clienteinicial)).then((snapshot) => {

        if (snapshot.exists()) {
            remove(refS(db, 'Comisiones/' + clienteinicial))
            .then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            })
            .catch((error) => {
                mostrarMensaje("Ocurrió un error al eliminar el producto:  " + error);
            });
        }else {
            limpiarInputs();
            mostrarMensaje("El producto don ID " + clienteinicial + "no existe.");

        }
    });
    Listarproductos();
}

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];
     
        if (file) {
            const storageRef = ref(storage, file.name);
            const uploadTask = uploadBytesResumable(storageRef, file);
            uploadTask.on('state_changed', (snapshot) => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
            }, (error) => {
                console.error(error);
                
            }, () => {
                getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
                    txtUrlInput.value = downloadUrl;
                    setTimeout(() => {
                        progressDiv.textContent = '';
                    }, 500);
                }).catch((error) => {
                    console.error(error);

                })
            });
        }
});


const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarComisiones);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarComisiones);